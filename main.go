package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

// Task  : structure composée de trois champs : Description (string), Done(bool), id (string)
type Task struct {
	Description string
	Done        bool
	id          string //attention : commence à 0
}

// task : variable globale : slice composé de Task
var task []Task

// main : fonction principale (utilisée pour appeler les autres fonctions)
func main() {
	//ajout tâches de test
	var nouvelle_tache1 Task
	nouvelle_tache1.Description = "Faire les courses"
	nouvelle_tache1.Done = true
	task = append(task, nouvelle_tache1)
	var nouvelle_tache2 Task
	nouvelle_tache2.Description = "Ranger la maison"
	nouvelle_tache2.Done = false
	task = append(task, nouvelle_tache2)
	/*mux1 := http.NewServeMux()
	mux2 := http.NewServeMux()
	mux3 := http.NewServeMux() je n'ai pas réussi à le faire fonctionner*/
	http.HandleFunc("/", list)     //permet d'afficher une liste des tâches
	http.HandleFunc("/done", done) //permet, selon le type de requête, soit d'afficher une liste des tâches terminées soit d'en marquer une comme terminée en passant son id à la requête
	http.HandleFunc("/add", add)   //permet d'ajouter une tâche en indiquant sa description
	/*mux1.HandleFunc("/", list)
	mux2.HandleFunc("/done", done)
	mux3.HandleFunc("/add", add)*/
	http.ListenAndServe("localhost:8080", nil) //lance le serveur web sur le port 80
	/*http.ListenAndServe("localhost:8081", mux1) //list : port 8081
	http.ListenAndServe("localhost:8082", mux2) //add : port 8082
	http.ListenAndServe("localhost:8083", mux3) //done : port 8083 je n'ai pas réussi à le faire fonctionner*/
}

// add : fonction permettant d'ajouter une tâche grâce à la description passée en paramètre (vérifie d'abord si on a bien utilisé le bon type de requête puis appelle la bonne fonction si c'est le cas)
//  @param rw : ce qui sera renvoyé au client
//  @param r ; ce qui a été envoyé par le client
func add(rw http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodPost { //vérifie si on a bien utilisé la bonne méthode HTTP
		addTask(rw, r)                //appelle la fonction addTask
		rw.WriteHeader(http.StatusOK) //indique que la requête a bien fonctionné

	} else {
		rw.Write([]byte("The HTTP Method used isn't the right one (should be POST"))
		rw.WriteHeader(http.StatusBadRequest) //indique au client que sa requête est incorrecte
	}
}

// addTask : fonction appelée par add() permettant d'ajouter une tâche grâce à son
//  @param rw : ce qui sera renvoyé au client
//  @param r ; ce qui a été envoyé par le client
func addTask(rw http.ResponseWriter, r *http.Request) {
	body := getBody(rw, r)
	bodyAsString := string(body)
	var newTask Task
	newTask.Description = bodyAsString
	newTask.id = strconv.Itoa(len(task) + 1)
	task = append(task, newTask) //ajoute la nouvelle tâche
}

// getBody : lit le contenu de la requête HTTP
//  @param rw : ce qui sera renvoyé au client
//  @param r ; ce qui a été envoyé par le client
//  @return []byte
func getBody(rw http.ResponseWriter, r *http.Request) []byte {
	var body []byte
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Printf("Error reading body: %v", err)
		http.Error(
			rw,
			"can't read body", http.StatusBadRequest, //indique au client qu'il y a une erreur dans la requête
		)
	}
	return body
}

// done : permet, selon le type de requête, soit d'afficher une liste des tâches terminées soit d'en marquer une comme terminée en passant son id à la requête
//  @param rw : ce qui sera renvoyé au client
//  @param r ; ce qui a été envoyé par le client
func done(rw http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case http.MethodGet:
		showDone(rw) //appelle la fonction showDone et affiche les tâches terminées

	case http.MethodPost:
		addDone(rw, r) //appelle la fonction addDone et permet de marquer une tâche terminée

	default: //si requête incorrecte : prévient le client
		http.Error(
			rw,
			"can't read body", http.StatusBadRequest, //indique au client qu'il y a une erreur dans la requête
		)
	}
}

// addDone : permet d'ajouter une nouvelle tâche
//  @param rw : ce qui sera renvoyé au client
//  @param r ; ce qui a été envoyé par le client
func addDone(rw http.ResponseWriter, r *http.Request) {
	body := getBody(rw, r)
	idDone := string(body)
	idDoneInt, _ := strconv.Atoi(idDone)
	if idDoneInt < len(task) {
		task[idDoneInt].Done = true   //change l'état de la tâche
		rw.WriteHeader(http.StatusOK) //indique que la requête a bien fonctionné
	}
}

// showDone : affiche les tâches terminées
//  @param rw : ce qui sera renvoyé au client
func showDone(rw http.ResponseWriter) {
	for id := range task {
		if task[id].Done == true { //vérifie l'état de chaque tâche (si l'état Done est à true)
			taskAsJson, _ := json.Marshal(task[id])
			rw.WriteHeader(http.StatusOK) //indique que la requête a bien fonctionné
			rw.Write(taskAsJson)          //affiche la tâche
		}
	}
}

// list : liste toutes les tâches
//  @param rw : ce qui sera renvoyé au client
//  @param _ : ne fait rien avec la requête http
func list(rw http.ResponseWriter, _ *http.Request) {
	taskAsJson, _ := json.Marshal(task)
	rw.WriteHeader(http.StatusOK) //indique que la requête a bien fonctionné
	rw.Write(taskAsJson)          //affiche la tâche
}
